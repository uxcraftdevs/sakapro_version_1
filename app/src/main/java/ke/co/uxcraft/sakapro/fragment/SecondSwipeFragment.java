package ke.co.uxcraft.sakapro.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ke.co.uxcraft.sakapro.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SecondSwipeFragment extends Fragment {


    public SecondSwipeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second_swipe, container, false);
    }

}
