package ke.co.uxcraft.sakapro.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import ke.co.uxcraft.sakapro.MainActivity;
import ke.co.uxcraft.sakapro.R;
import ke.co.uxcraft.sakapro.activity.CustomiseYourDashboard;

/**
 * A simple {@link Fragment} subclass.
 */
public class ThirdSwipeFragment extends Fragment {


    public ThirdSwipeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_third_swipe, container, false);
        Button buttonGetstarted=view.findViewById(R.id.buttonGetStarted);
        buttonGetstarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CustomiseYourDashboard.class));
            }
        });

        return view;


    }

}
