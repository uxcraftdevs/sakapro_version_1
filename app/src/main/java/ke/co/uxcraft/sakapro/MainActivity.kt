package ke.co.uxcraft.sakapro

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import android.view.Window
import android.view.WindowManager
import ke.co.uxcraft.sakapro.activity.CustomiseYourDashboard
import ke.co.uxcraft.sakapro.adapter.ViewPagerAdapter
import ke.co.uxcraft.sakapro.fragment.FirstSwipedFragment
import ke.co.uxcraft.sakapro.fragment.SecondSwipeFragment
import ke.co.uxcraft.sakapro.fragment.ThirdSwipeFragment
import ke.co.uxcraft.sakapro.util.ZoomOutPageTransformer
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_third_swipe.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main)

        setUpViewpagerAndTablLayout(viewPager = viewpager)


    }

    fun setUpViewpagerAndTablLayout(viewPager: ViewPager) {

        val adapter = ViewPagerAdapter(supportFragmentManager)

        // Add Fragments to adapter one by one
        adapter.addFragment(FirstSwipedFragment(), null)
        adapter.addFragment(SecondSwipeFragment(),null)
        adapter.addFragment(ThirdSwipeFragment(),null)
        viewPager.adapter = adapter
        viewPager.setPageTransformer(true, ZoomOutPageTransformer())

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {/*empty*/
            }

            override fun onPageSelected(position: Int) {
                pageIndicatorView.selection = position
            }

            override fun onPageScrollStateChanged(state: Int) {/*empty*/
            }
        })

    }
}
